#!/bin/bash

#Configure Your Email
recipient_email="mymail@example.com"
smtp_server="smtp.your_email_provider.com"
smtp_port="587"  # Adjust port if needed (e.g., 465 for TLS)
smtp_username="username"
smtp_password="password"

send_email="${send_email:-false}"

#declare a log file
logfile="scan_results.txt"

#loop through each IP in the provided list
while ifs= read -r ip; do

#Get Current Timestamp
 timestamp=$(date +"%Y-%m-%d %H:%M:%S")

#Ping the Host 5 times and capture output

    ping -c 5 $ip

#Check if results are succesfull and set the status
if [[ $? -eq 0 ]]; then #exit code 0 indicates success
    status="Alive"
else
    status="Unreachable"

#Emailing logic if variable send_email=true (false if not provided)
    if [[ "send_email" == "true" ]]; then
        subject="Alert: Node $ip Unreachable!!"
        body="I have detected that $ip node is unreachable at $timestamp"

        echo -e "Subject: $subject\n\n$body" | sendmail -h "$smtp_server" -p "$smtp_port" -xu "$smtp_username" -xp "$smtp_password" -t "$recipient_email" &> /dev/null
    fi 
fi

# Log the timestamp, IP address, status, and ping output
  echo "$timestamp - $ip: $status" >> "$logfile"
  echo "" >> "$logfile"  # Add a blank line for separation
done < "$1"

#Error Handling
if [[ ! -f "$1" ]]; then
  echo "Error: No IP's were provided!"
  exit 1
fi
